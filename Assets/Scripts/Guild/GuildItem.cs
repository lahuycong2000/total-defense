using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace ZomDefense
{
    public class GuildItem : MonoBehaviour
    {
        [SerializeField] private Image _imageData;
        [SerializeField] private List<string> _level1Item = new List<string>();
        [SerializeField] private List<string> _level2Item = new List<string>();
        [SerializeField] private List<string> _level3Item = new List<string>();
        [SerializeField] private string _inforItem;
        public void InitItem(Sprite image, List<string> level1, List<string> level2, List<string> level3, string infor)
        {
            _imageData.sprite = image;
            SetData(level1, _level1Item);
            SetData(level2, _level2Item);
            SetData(level3, _level3Item);
            _inforItem = infor;
        }
        public void SetData(List<string> list, List<string> text)
        {
            for (int i = 0; i < list.Count; i++)
            {
                text[i] = list[i];
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace ZomDefense
{
    public class GuildManager : MonoBehaviour
    {
        public const string PATH_ITEM = "Data/{0}";
        [Header("DATA")]
        [SerializeField] private Image _imageData;
        [SerializeField] private List<TextMeshProUGUI> _level1 = new List<TextMeshProUGUI>();
        [SerializeField] private List<TextMeshProUGUI> _level2 = new List<TextMeshProUGUI>();
        [SerializeField] private List<TextMeshProUGUI> _level3 = new List<TextMeshProUGUI>();
        [SerializeField] private TextMeshProUGUI _infor;

        [Header("ITEM")]
        [SerializeField] private GuildItem _prefabItem;

        public void Init(Sprite image, List<string> level1, List<string> level2, List<string> level3, string infor)
        {
            _imageData.sprite = image;
            SetData(level1, _level1);
            SetData(level2, _level2);
            SetData(level3, _level3);
            _infor.text = infor;
        }
        public void SetData(List<string> list, List<TextMeshProUGUI> text)
        {
            for (int i = 0; i < list.Count; i++)
            {
                text[i].text = list[i];
            }
        }
        public void Instantiate()
        {
            for (int i = 0; i < Database.Instance.turrets.Length; i++)
            {
                GuildItem item = Instantiate(_prefabItem);
                Turret data = Database.Instance.turrets[i];
                //item.InitItem(LoadSprite(data.dataTurret[0].image),SetData(item.level1,0))
            }
        }
        public void SetData(List<string> list, int index)
        {
            Turret data = Database.Instance.turrets[index];
            list.Add(data.dataTurret[index].damage.ToString());
            list.Add(data.dataTurret[index].damage.ToString());
            list.Add(data.dataTurret[index].damage.ToString());
        }
        private Sprite LoadSprite(string nameImage)
        {
            string path = string.Format(PATH_ITEM, nameImage);
            Sprite sprite = Resources.Load<Sprite>(path);
            return sprite;
        }

    }
}

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace ZomDefense
{
    public class ButtonChooseLevel : MonoBehaviour
    {
        [SerializeField] private int _level;
        [SerializeField] private Button _btn;

        private Action<int> _onClicked;
        public Action<int> OnClicked { get => _onClicked; set => _onClicked = value; }

        private void Awake()
        {
            _btn.onClick.AddListener(() =>
            {
                _onClicked?.Invoke(_level);
            });
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Ensign.Unity;
namespace ZomDefense
{
    public class LoadingScene : MonoBehaviour
    {
        public GameObject _loadingScreen;
        public Slider _loadingBarFill;
        public void LoadScene()
        {
            _loadingBarFill.value = 0;
            _loadingScreen.SetActive(true);
            StartCoroutine(LoadSceneAsync());
        }

        IEnumerator LoadSceneAsync()
        {
            while (_loadingBarFill.value != _loadingBarFill.maxValue)
            {
                float progressValue = 1;
                _loadingBarFill.value += progressValue;
                yield return null;
            }
            this.ActionWaitTime(2, () =>
            {
                _loadingScreen.SetActive(false);
                //GameManager.Instance.SetUp();

            });
        }
    }
}


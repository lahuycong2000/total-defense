using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretUI : MonoBehaviour
{
    [SerializeField] private Image _imageTurret;
    private string _nameTurret;
    private int _describeTurret;
    private int _reloadingTime;
    private int _damage;
    private int _range;
    private int _special;

    public void Init(Image image, string name, int describe, int reloading, int damage, int range, int special)
    {
        _imageTurret = image;
        _nameTurret = name;
        _describeTurret = describe;
        _reloadingTime = reloading;
        _damage = damage;
        _range = range;
        _special = special;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ZomDefense
{
    public class PopupSelect : MonoBehaviour
    {
        [SerializeField] private List<ButtonTab> _buttonTabs;
        [SerializeField] private List<ButtonChooseLevel> _buttonLevel;
        private void OnEnable()
        {
            _buttonLevel.ForEach(btn => btn.OnClicked += UIManager.Instance.ChooseLevelGame);
            _buttonTabs.ForEach(btn => btn.OnClicked += UIManager.Instance.ChangeTab);
        }
    }
}

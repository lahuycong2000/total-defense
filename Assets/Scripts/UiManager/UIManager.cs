using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign.Unity;
using TMPro;
namespace ZomDefense
{
    public class UIManager : Singleton<UIManager>
    {
        [Header("REST_TRANSFORM")]
        [SerializeField] private Transform _mainScene;
        [SerializeField] private Transform _playScene;
        [SerializeField] private Transform _chooseLevelScene;
        [SerializeField] private Transform _skillScene;
        [SerializeField] private Transform _guideScene;
        [SerializeField] private Transform _creditScene;

        [Header("Popup")]
        [SerializeField] private Transform _sceneGame;
        [SerializeField] private LoadingScene _loadingScene;
        [SerializeField] private GameObject _popupNewGame;


        [SerializeField] private GameObject _popupWinGame;
        [SerializeField] private GameObject _popupLoseGame;
        [SerializeField] private GameObject _popupPause;

        [Header("OTHERS")]
        [SerializeField] private List<Image> _heartList = new List<Image>();
        [SerializeField] private int _levelChoose;
        [SerializeField] private TextMeshProUGUI _txtWave;

        [Header("CoinManager")]
        [SerializeField] private TextMeshProUGUI _textCoin;
        [SerializeField] private int _coin;
        [Header("GemManager")]
        [SerializeField] private TextMeshProUGUI _textGem;
        [SerializeField] private int _gem;


        public List<Image> HeartList { get => _heartList; set => _heartList = value; }
        public int LevelChoosen { get => _levelChoose; set => _levelChoose = value; }
        public GameObject PopupWinGame { get => _popupWinGame; set => _popupWinGame = value; }
        public GameObject PopupLoseGame { get => _popupLoseGame; set => _popupLoseGame = value; }
        public GameObject PopupPause { get => _popupPause; set => _popupPause = value; }

        public LoadingScene LoadingScene { get => _loadingScene; set => _loadingScene = value; }
        public Transform SceneGame { get => _sceneGame; set => _sceneGame = value; }
        public int Coin { get => _coin; set => _coin = value; }
        public int Gem { get => _gem; set => _gem = value; }

        private EMainUI _currentTab;
        public EMainUI CurrentTab
        {
            get => _currentTab;
            set
            {
                _currentTab = value;
                UpdateUI(_currentTab);
                switch (_currentTab)
                {
                    case EMainUI.MainScene:
                        break;
                    case EMainUI.PlayScene:
                        
                        GameManager.Instance.SelectLevel(_levelChoose);
                        _sceneGame.gameObject.SetActive(true);
                        GameManager.Instance.GameState = GameManager.EGameState.StartGame;

                        //AdsManager.Instance.ShowAdmobBanner(true);
                        break;
                    case EMainUI.ChooseLevelScene:
                        break;
                    case EMainUI.SkillScene:
                        break;
                    case EMainUI.GuideScene:
                        break;
                    case EMainUI.PopupNewGame:
                        _mainScene.gameObject.SetActive(_currentTab != EMainUI.MainScene);
                        break;
                    case EMainUI.PopupPause:
                        _playScene.gameObject.SetActive(_currentTab != EMainUI.PlayScene);
                        GameManager.Instance.GameState = GameManager.EGameState.Pause;
                        break;
                }
            }
        }

        void Start()
        {
            //_levelChoose = 0;
            _coin = 0;
            //UpdateUiCoinGem(_textCoin, _coin);
        }

        private void OnEnable()
        {
            ChangeTab(EMainUI.MainScene);
        }
        private void OnDisable()
        {

        }
        public void UIWave(string currentWave, string maxWave)
        {
            _txtWave.text = $"{currentWave}/{maxWave}";
        }
        public void ChooseLevelGame(int number)
        {  
            _levelChoose = number;
            ChangeTab(EMainUI.PlayScene);
        }
        public void ChangeTab(EMainUI data)
        {
            CurrentTab = data;
        }
        public void Loading()
        {
            _loadingScene.gameObject.SetActive(true);
            _loadingScene.LoadScene();
        }
        public void UpdateUI(EMainUI tab)
        {
            if (tab != EMainUI.PopupNewGame && tab != EMainUI.PopupPause && tab != EMainUI.PlayScene)
            {
                Loading();
            }
            _playScene.gameObject.SetActive(tab == EMainUI.PlayScene);
            _mainScene.gameObject.SetActive(tab == EMainUI.MainScene);
            _chooseLevelScene.gameObject.SetActive(tab == EMainUI.ChooseLevelScene);
            _guideScene.gameObject.SetActive(tab == EMainUI.GuideScene);
            _skillScene.gameObject.SetActive(tab == EMainUI.SkillScene);
            _creditScene.gameObject.SetActive(tab == EMainUI.CreditScene);
            _popupPause.gameObject.SetActive(tab == EMainUI.PopupPause);
            _popupNewGame.gameObject.SetActive(tab == EMainUI.PopupNewGame);

        }
        public void TweenMove(PopupSelect currentPopup, Vector3 cellSelected, float time)
        {
            currentPopup.gameObject.transform.TweenMove(cellSelected, time);
        }
        public void BackMain()
        {
            //AdsManager.Instance.ShowAdmobBanner(false);
            GameManager.Instance.ResetDataGame();
            CurrentTab = EMainUI.ChooseLevelScene;
            //ChangeTab(EMainUI.ChooseLevelScene);
        }
        public void ResetGame()
        {
            // AdsManager.Instance.ShowAdmobBanner(false);
            GameManager.Instance.ResetDataGame();
            CurrentTab = EMainUI.PlayScene;
            //ChangeTab(EMainUI.PlayScene);
        }
        public void SetPopupGame(GameManager.EGameState current)
        {
            _popupPause.gameObject.SetActive(current == GameManager.EGameState.Pause);
            _popupWinGame.gameObject.SetActive(current == GameManager.EGameState.WinGame);
            _popupLoseGame.gameObject.SetActive(current == GameManager.EGameState.LoseGame);
        }
        public void UpdateCoinStart(int number)
        {
            _coin = number;
            UpdateUiCoinGem(_textCoin, _coin);
        }
        public void AddCoin(int number)
        {
            _coin += number;
            UpdateUiCoinGem(_textCoin, _coin);
        }
        public void SubtractCoin(int number)
        {
            _coin -= number;
            UpdateUiCoinGem(_textCoin, _coin);
        }


        public void AddGem(int number)
        {
            _gem += number;
            UpdateUiCoinGem(_textGem, _gem);
        }
        public void SubtractGem(int number)
        {
            _gem -= number;
            UpdateUiCoinGem(_textGem, _gem);
        }
        public void UpdateUiCoinGem(TextMeshProUGUI textNumber, int number)
        {
            textNumber.text = number.ToString();
        }
    }
    public enum EMainUI
    {
        MainScene,
        PlayScene,
        ChooseLevelScene,
        SkillScene,
        GuideScene,
        CreditScene,
        PopupPause,
        PopupNewGame,
        ClosePopup,
    }
}

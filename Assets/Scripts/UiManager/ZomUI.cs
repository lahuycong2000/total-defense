using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZomUI : MonoBehaviour
{
    [SerializeField] private Image _imageZom;
    private string _nameZom;
    private int _describeZom;
    private int _reloadingTime;
    private int _damage;
    private int _range;
    private int _special;

    public void Init(Image image, string name, int describe, int reloading, int damage, int range, int special)
    {
        _imageZom = image;
        _nameZom = name;
        _describeZom = describe;
        _reloadingTime = reloading;
        _damage = damage;
        _range = range;
        _special = special;
    }
}

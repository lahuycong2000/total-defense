using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace ZomDefense
{
    public class PopupController : MonoBehaviour
    {
        [SerializeField] private GameObject _popup;
        [SerializeField] private Button _btnContinue;
        [SerializeField] private Button _btnReset;
        [SerializeField] private Button _btnExit;

        private void OnEnable()
        {
            if (_btnContinue != null)
            {
                _btnContinue.onClick.AddListener(CloseContiune);
            }
            _btnReset.onClick.AddListener(CloseReset);
            _btnExit.onClick.AddListener(CloseExit);
        }
        public void Start()
        {
             //AdsManager.Instance.ShowNormalAd(GameManager.Instance.GameState);
        }
        public void CloseContiune()
        {
            GameManager.Instance.GameState = GameManager.EGameState.Playing;
            _popup.SetActive(false);
        }
        public void CloseReset()
        {
            UIManager.Instance.ResetGame();
            _popup.SetActive(false);
        }
        public void CloseExit()
        {
            //AdsManager.Instance.ShowAdmobBanner(false);
            UIManager.Instance.BackMain();
            _popup.SetActive(false);
        }
    }
}

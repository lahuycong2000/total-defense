using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace ZomDefense
{
    public class ButtonTab : MonoBehaviour
    {
        [SerializeField] private EMainUI _typeMainUI;
        [SerializeField] private Button _btn;

        private Action<EMainUI> _onClicked;
        public Action<EMainUI> OnClicked { get => _onClicked; set => _onClicked = value; }

        private void Awake()
        {
            _btn.onClick.AddListener(() =>
            {
                _onClicked?.Invoke(_typeMainUI);
                GameManager.Instance.ManagerSfx.PlayEffect();
            });
        }
    }
}

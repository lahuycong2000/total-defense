using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Ensign;

namespace ZomDefense
{
    public class CameraSizeController : MonoBehaviour
    {
        [Space, Header("Camera")]
        [SerializeField] private Camera _refCam;
        [Space, Header("Resize Data")]
        [SerializeField] private Vector2 _refAspect;
        [SerializeField] private float _refCamOrthoSize;

        private void Start()
        {
            float refAspect = _refAspect.x / _refAspect.y;
            if (refAspect <= _refCam.aspect) return;
            _refCam.orthographicSize = (_refCamOrthoSize * refAspect) / _refCam.aspect;
        }
    }
}



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace ZomDefense
{
    public class SoundManager : MonoBehaviour
    {
        private static SoundManager Instance;
        private bool _playMusicOnStart = true;
        private AudioClip _musicsGame;
        [Range(0, 1)]
        private float _musicsGameVolume = 0.5f;
        private AudioClip _soundClick;

        private AudioSource _musicAudio;
        private AudioSource _soundFx;

        //GET and SET
        public static float MusicVolume
        {
            set { Instance._musicAudio.volume = value; }
            get { return Instance._musicAudio.volume; }
        }
        public static float SoundVolume
        {
            set { Instance._soundFx.volume = value; }
            get { return Instance._soundFx.volume; }
        }

        public static void ResetMusic()
        {
            Instance._musicAudio.Stop();
            Instance._musicAudio.Play();
        }

        public void PauseMusic(bool isPause)
        {
            if (isPause)
                Instance._musicAudio.mute = true;
            else
                Instance._musicAudio.mute = false;
        }

        public static void Click()
        {
            PlaySfx(Instance._soundClick, 1);
        }

        void Awake()
        {
            Instance = this;
            _musicAudio = gameObject.AddComponent<AudioSource>();
            _musicAudio.loop = true;
            _musicAudio.volume = 0.5f;
            _soundFx = gameObject.AddComponent<AudioSource>();

            if (_playMusicOnStart)
            {
                PlayMusic(_musicsGame, _musicsGameVolume);
                Debug.Log("===============");
            }
            Debug.Log(">>>>>>>>>>>>>");
        }

        public static void PlayGameMusic()
        {
            PlayMusic(Instance._musicsGame, MusicVolume);
        }

        public static void PlaySfx(AudioClip clip)
        {
            Instance.PlaySound(clip, Instance._soundFx);
        }

        public static void PlaySfx(AudioClip[] clips)
        {
            if (Instance != null && clips.Length > 0)
                Instance.PlaySound(clips[Random.Range(0, clips.Length)], Instance._soundFx);
        }

        public static void PlaySfx(AudioClip[] clips, float volume)
        {
            if (Instance != null && clips.Length > 0)
                Instance.PlaySound(clips[Random.Range(0, clips.Length)], Instance._soundFx, volume);
        }

        public static void PlaySfx(AudioClip clip, float volume)
        {
            Instance.PlaySound(clip, Instance._soundFx, volume);
        }

        public static void PlayMusic(AudioClip clip, bool loop = true)
        {
            if (Instance != null)
                return;

            Instance._musicAudio.loop = loop;
            Instance.PlaySound(clip, Instance._musicAudio);
        }

        public static void PlayMusic(AudioClip clip, float volume)
        {
            Instance.PlaySound(clip, Instance._musicAudio, volume);
        }

        private void PlaySound(AudioClip clip, AudioSource audioOut)
        {
            if (clip == null)
            {
                return;
            }

            if (audioOut == _musicAudio)
            {
                audioOut.clip = clip;
                audioOut.Play();
            }
            else
                audioOut.PlayOneShot(clip, SoundVolume);
        }

        private void PlaySound(AudioClip clip, AudioSource audioOut, float volume)
        {
            if (clip == null)
            {
                return;
            }

            if (audioOut == _musicAudio)
            {
                audioOut.clip = clip;
                audioOut.Play();
                audioOut.volume = volume;

            }
            else
                audioOut.PlayOneShot(clip, SoundVolume * volume);
        }


    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ZomDefense
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private int _idLevel;
        [SerializeField] private int _idWave;
        [SerializeField] private List<Transform> _listPoint = new List<Transform>();
        [SerializeField] private GameObject _enemyPos;
        [SerializeField] private GameObject _turretPos;
        [SerializeField] private bool _isEnd;
        [SerializeField] private List<int> _listIdEnemy = new List<int>();
        [SerializeField] private List<TurretManager> _listTurret = new List<TurretManager>();

        public List<Transform> ListPoint { get => _listPoint; set => _listPoint = value; }
        public GameObject EnemyPos { get => _enemyPos; set => _enemyPos = value; }
        public GameObject TurretPos { get => _turretPos; set => _turretPos = value; }
        public int Idlevel { get => _idLevel; set => _idLevel = value; }
        public int IdWave { get => _idWave; set => _idWave = value; }

        
        public List<int> ListIdEnemy { get => _listIdEnemy; set => _listIdEnemy = value; }
        public List<TurretManager> ListTurret { get => _listTurret; set => _listTurret = value; }

        public void Start()
        {     
            SetIdWave();
            UIManager.Instance.UIWave(_idWave.ToString(),Database.Instance.levels[_idLevel-1].waves.Length.ToString());
        }
        public void SetIdWave()
        {
            _isEnd = false;
            _listIdEnemy.Clear();
            Wave data = Database.Instance.levels[_idLevel - 1].waves[_idWave];
            for (int i = 0; i < data.idEnemies.Length; i++)
            {
                for (int j = 0; j < data.idEnemies[i].count; j++)
                {
                    _listIdEnemy.Add(data.idEnemies[i].idEnemy - 1);
                }
            }
        }
        public void CheckLevel()
        {
            _idWave++;
            UIManager.Instance.UIWave(_idWave.ToString(),Database.Instance.levels[_idLevel-1].waves.Length.ToString());
            if (_idWave == Database.Instance.levels[_idLevel - 1].waves.Length)
            {
                _isEnd = true;
                //CheckLevel();
            }
            else
            {
                SetIdWave();
            }
        }
        public void CheckWinGame()
        {
            if (_isEnd == true)
            {
                GameManager.Instance.GameState = GameManager.EGameState.WinGame;
            }
        }

    }
}

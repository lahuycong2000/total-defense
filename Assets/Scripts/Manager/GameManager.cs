using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ensign.Unity;
namespace ZomDefense
{
    public class GameManager : Singleton<GameManager>
    {
        public const string PATH_ITEM = "Data/{0}";
        [SerializeField] private List<LevelManager> _listLevelScene = new List<LevelManager>();
        [SerializeField] private List<Enemy> _listEnemy = new List<Enemy>();
        [SerializeField] private Canvas _canvas;
        [SerializeField] private Enemy _prefabEnemy;
        [SerializeField] private List<Bullet> _prefabBullet;
        [SerializeField] private TurretItem _prefabTurret;
        [SerializeField] private int _index;

        [SerializeField] private LevelManager _levelManager;
        [SerializeField] private SfxManager _sfxManager;

        private bool _isStartWave;
        private int _countHeart;

        public LevelManager LvlManager { get => _levelManager; set => _levelManager = value; }
        public List<Enemy> ListEnemy { get => _listEnemy; set => _listEnemy = value; }
        public SfxManager ManagerSfx { get => _sfxManager; set => _sfxManager = value; }
        public List<Bullet> PrefabBullet { get => _prefabBullet; set => _prefabBullet = value; }

        private EGameState _gameState;

        private float _countTime;

        public EGameState GameState
        {
            get
            {
                return _gameState;
            }
            set
            {
                _gameState = value;
                switch (_gameState)
                {
                    case EGameState.MainMenu:
                        break;
                    case EGameState.ChooseLevel:
                        break;
                    case EGameState.StartGame:
                        ;
                        _prefabEnemy.CreatePool(50);
                        _prefabBullet[0].CreatePool<Bullet>(30);
                        _prefabBullet[1].CreatePool<Bullet>(30);
                        _prefabBullet[2].CreatePool<Bullet>(30);
                        _prefabTurret.CreatePool<TurretItem>(10);
                        StartGame();
                        //GameState = EGameState.Playing;
                        break;
                    case EGameState.Playing:
                        //TEstPlay();
                        break;
                    case EGameState.Pause:
                        UIManager.Instance.SetPopupGame(EGameState.Pause);
                        break;
                    case EGameState.WinGame:
                        _canvas.sortingOrder = 0;
                        UIManager.Instance.SetPopupGame(EGameState.WinGame);
                        break;
                    case EGameState.LoseGame:
                        _canvas.sortingOrder = 0;
                        UIManager.Instance.SetPopupGame(EGameState.LoseGame);
                        break;
                    case EGameState.ClosePopup:
                        UIManager.Instance.SetPopupGame(EGameState.ClosePopup);
                        break;
                }
            }
        }
        public void StartGame()
        {
            _canvas.sortingOrder = 1;
            _index = 0;
            _countTime = 0;
            _countHeart = 0;
            _listEnemy.Clear();
            Heart();
            TEstPlay();
            //GameState = EGameState.ChooseLevel;
        }

        public void Heart()
        {
            foreach (var item in UIManager.Instance.HeartList)
            {
                item.gameObject.SetActive(true);
            }
        }
        public void HeartDie()
        {
            for (int i = 0; i < UIManager.Instance.HeartList.Count; i++)
            {
                UnityEngine.UI.Image item = UIManager.Instance.HeartList[i];
                if (item.gameObject.activeSelf == true)
                {
                    item.gameObject.SetActive(false);
                    _countHeart++;
                    LossGame();
                    break;
                }
            }
        }
        public void LossGame()
        {
            if (_countHeart >= 3)
            {
                GameState = EGameState.LoseGame;
            }
        }
        public void TEstPlay()
        {
            GameState = EGameState.Playing;
            _isStartWave = true;
        }
        public void EndWave()
        {
            _index = 0;
            _levelManager.CheckLevel();
        }
        public void SelectLevel(int number)
        {
            //_levelManager = Instantiate(_listLevelScene[number - 1], Vector3.zero, Quaternion.identity, UIManager.Instance.SceneGame.transform);
            Debug.Log(Database.Instance.levels[number - 1].startCoin);
            UIManager.Instance.UpdateCoinStart(Database.Instance.levels[number - 1].startCoin);
            _levelManager = _listLevelScene[number-1].Spawn(UIManager.Instance.SceneGame.transform);
            _levelManager.IdWave = 0;
        }
        public void ResetDataGame()
        {
            UIManager.Instance.SceneGame.DestroyAllChilren();
        }
        public void Update()
        {
            if (GameState == EGameState.Playing)
            {
                if (_isStartWave == false && _listEnemy.Count == 0)
                {
                    _levelManager.CheckWinGame();
                }
                if (_isStartWave == true)
                {
                    StartCoroutine(SpawnWave());
                    SpawnCount();
                }
            }

        }
        public void SpawnCount()
        {
            if (_countTime <= 1)
            {
                _countTime += Time.deltaTime;
                // _countTime++;   
            }
        }
        public Enemy SpawnEnemy(int number)
        {
            Enemy enemy = _prefabEnemy.Spawn(_levelManager.EnemyPos.transform);
            DataEnemy data = Database.Instance.enemys[number];
            enemy.Init(data._imageEnemy, data.health, data.speed, data.coin);
            SetAnim(enemy, data.nameEnemy);
            //enemy.Init(LoadSprite(data.image), data.health, data.speed, data.coin);
            _listEnemy.Add(enemy);
            return enemy;
        }
        public void SetAnim(Enemy enemy, string nameAnim)
        {
            switch (nameAnim)
            {
                case "Goblin":
                    enemy.Animator.SetTrigger("isGoblin");
                    break;
                case "Bat":
                    enemy.Animator.SetTrigger("isBat");
                    break;
                case "Orc":
                    enemy.Animator.SetTrigger("isOrc");
                    break;
            }
        }

        IEnumerator SpawnWave()
        {
            yield return new WaitUntil(() => _countTime >= 1);
            _index++;
            //Debug.Log(_levelManager.ListIdEnemy[_index - 1]);
            SpawnEnemy(_levelManager.ListIdEnemy[_index - 1]);
            CheckEndWave();
            _countTime = 0;
        }
        public void CheckEndWave()
        {
            if (_index == _levelManager.ListIdEnemy.Count)
            {
                _isStartWave = false;
                this.ActionWaitTime(10, () =>
            {
                TEstPlay();

            });
                EndWave();
                //GameState = EGameState.EndWave;
            }
        }
        public TurretItem SpawnTurret(Vector3 vector3)
        {
            TurretItem turret = _prefabTurret.Spawn(_levelManager.TurretPos.transform, vector3, Quaternion.identity);
            //_listTurret.Add(turret);
            return turret;
        }

        private Sprite LoadSprite(string nameImage)
        {
            string path = string.Format(PATH_ITEM, nameImage);
            Sprite sprite = Resources.Load<Sprite>(path);
            return sprite;
        }
        public enum EGameState
        {
            MainMenu,
            ChooseLevel,
            StartGame,
            Pause,
            Playing,
            WinGame,
            LoseGame,
            ClosePopup,
        }
    }
}
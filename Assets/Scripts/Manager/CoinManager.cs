using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
namespace DoWork
{
    public class CoinManager : MonoBehaviour
    {
        [SerializeField] private int _currentCoin;
        [SerializeField] private TextMeshProUGUI _textCurrentCoin;
        [SerializeField] private TextMeshProUGUI _textCurrentName;
        private int _index;
        public int Index { get => _index; set => _index = value; }


        private void Start()
        {
            UpdateInfo();
        }

        public void UpdateInfo()
        {
            //_currentCoin = GameManager.Instance.LoadSaveJson.TakeCoin();
            _textCurrentCoin.text = String.Format("{0:#,0} đ", _currentCoin);
           // _textCurrentName.text = GameManager.Instance.LoadSaveJson.TakeName();
        }
        public void AddCoin(int numberCoin)
        {
            _currentCoin += numberCoin;
            //GameManager.Instance.LoadSaveJson.SaveNewData(_currentCoin, 0);
            //GameManager.Instance.ManagerSfx.PlayEffect(GameManager.Instance.ManagerSfx.SfxCoin);
            //Debug.Log("++ " + numberCoin);
        }
        public void RemoveCoin(int numberCoin, int numberItem)
        {
            if (_currentCoin < numberCoin)
            {
                //GameManager.Instance.ManagerSfx.PlayEffect(GameManager.Instance.ManagerSfx.SfxNoMoney);
                _index = 0;
            }
            else
            {
                _currentCoin -= numberCoin;
                //GameManager.Instance.LoadSaveJson.SaveNewData(_currentCoin, numberItem);
                _index = 1;
                //Debug.Log("-- " + numberCoin);
            }
        }
    }
}


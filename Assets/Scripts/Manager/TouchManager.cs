using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ZomDefense
{
    public class TouchManager : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private TurretManager _itemSelect;
        [SerializeField] private LayerMask _layerMask;
        public void OnPointerClick(PointerEventData eventData)
        {
            if (Input.touchCount > 1)
                return;
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D[] hit = CheckOnClick();
            foreach (var item in hit)
            {
                // if (item.collider == null)
                // {
                //     Debug.Log("0");
                //     if (_itemSelect != null)
                //     {
                //         Debug.Log("1");
                //         SetUI(false);
                //     }
                //     return;
                // }

                if (item.collider.gameObject.GetComponent<TurretManager>() != null)
                {
                    if (_itemSelect != null)
                    {
                        SetUI(false);
                    }
                    //Debug.Log("2");
                    //Debug.Log("2");
                    _itemSelect = item.collider.gameObject.GetComponent<TurretManager>();
                    SetUI(true);

                }
                if (item.collider.gameObject.GetComponent<TurretUpdate>() != null)
                {
                    if (_itemSelect.NewTurret == true)
                    {
                        //Debug.Log("3");
                        CheckSelectTurret(item);
                    }
                }
            }
        }
        public void SetUI(bool isTrue)
        {
            _itemSelect.NewTurret.gameObject.SetActive(isTrue);
        }
        public void CheckSelectTurret(RaycastHit2D ray)
        {
            TurretUpdate turret = ray.collider.gameObject.GetComponent<TurretUpdate>();
            if (turret != null && !turret.IsSell)
            {
                if (UIManager.Instance.Coin < turret.PriceTurret)
                {
                    Debug.Log("ko du tien" + UIManager.Instance.Coin);
                    // Debug.Log("1ko du tien" + turret.PriceTurret);
                }
                else
                {
                    //Debug.Log("du tien");
                    UIManager.Instance.SubtractCoin(turret.PriceTurret);
                    _itemSelect.Id = turret.IdTurret;
                    _itemSelect.TurretLevelUp();
                    SetUI(false);
                }
            }
            else
            {
                turret.Sell(_itemSelect);
                SetUI(false);
                //Debug.Log("Ban");
            }
        }
        public RaycastHit2D[] CheckOnClick()
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D[] hits = Physics2D.RaycastAll(mousePos2D, Vector2.zero, _layerMask);
            if (hits.Length == 0 && _itemSelect != null)
            {
                SetUI(false);
            }
            return hits;
        }
    }
}

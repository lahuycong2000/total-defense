using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxManager : MonoBehaviour
{
    [SerializeField] private AudioSource _sourceAudio;
    [SerializeField] private AudioClip _sfxClick;

    public AudioClip SfxClick { get => _sfxClick; set => _sfxClick = value; }

    public void PlayEffect()
    {
        _sourceAudio.clip = _sfxClick;
        _sourceAudio.Play();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace ZomDefense
{
    public class TurretUpdate : MonoBehaviour
    {
        [SerializeField] private int _idTurret;
        [SerializeField] private Image _imageTurret;
        [SerializeField] private TextMeshProUGUI _price;
        private int _priceTurret;
        public bool IsSell;
        public int IdTurret { get => _idTurret; set => _idTurret = value; }
        public int PriceTurret { get => _priceTurret; set => _priceTurret = value; }
        public void Init(int id, Sprite image, int price)
        {
            _idTurret = id;
            _imageTurret.sprite = image;
            PriceTurret = price;
            _price.text = price.ToString();
        }
        public void UIcoin() 
        {
            _price.text = _priceTurret.ToString();
        }
        public void Sell(TurretManager turret)
        {
            turret.SellTurret(PriceTurret);
        }
    }
}

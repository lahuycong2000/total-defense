using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Ensign.Unity;
namespace ZomDefense
{
    public class TurretItem : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _imageTurret;
        [SerializeField] private float _rangeTurret;
        [SerializeField] private int _damageTurret;
        [SerializeField] private float _timeShoot;
        [SerializeField] private Bullet _bullet;
        [SerializeField] private GameObject _barrel;
        [SerializeField] private Enemy _enemyTarget;

        private float _timeNextShoot;
        public Enemy EnemyTarget { get => _enemyTarget; set => _enemyTarget = value; }

        private bool _clicked;
        public bool Clicked
        {
            get
            {
                return _clicked;
            }
            set
            {
                _clicked = value;
            }
        }
        public void Init(Sprite image, float range, int damage, float timeshoot, int index)
        {
            _imageTurret.sprite = image;
            _rangeTurret = range;
            _damageTurret = damage;
            _timeShoot = timeshoot;
            _bullet = GameManager.Instance.PrefabBullet[index];

        }
        
        void Start()
        {
            _clicked = false;
            _timeNextShoot = Time.time;
        }

        // Update is called once per frame
        void Update()
        {
            if (GameManager.Instance.GameState == GameManager.EGameState.Playing)
            {
                CheckEnemy();
                //TurretRotation();
                if (Time.time >= _timeNextShoot)
                {
                    IsShooting();
                }
            }
        }
        public void IsShooting()
        {
            if (_enemyTarget != null)
            {
                Shooting();
                _timeNextShoot = Time.time + _timeShoot;
            }
        }
        public void TurretRotation()
        {
            if (this != null)
            {
                if (this.EnemyTarget != null)
                {
                    Vector2 relation = this.EnemyTarget.transform.position - this.transform.position;
                    float angle = Mathf.Atan2(relation.x, relation.y) * Mathf.Rad2Deg;
                    this.transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);
                }
            }
        }
        public void CheckEnemy()
        {
            Enemy enemyNear = null;
            float distance = Mathf.Infinity;

            foreach (Enemy enemy in GameManager.Instance.ListEnemy)
            {
                float dis = (transform.position - enemy.transform.position).magnitude;
                if (dis < distance)
                {
                    distance = dis;
                    enemyNear = enemy;
                    // Debug.Log("=" + transform.position);
                    // Debug.Log("====" + enemy.transform.position);
                    // CheckFlixEnemy(enemy);
                }
            }

            if (distance <= _rangeTurret)
            {
                _enemyTarget = enemyNear;
                CheckFlixEnemy(enemyNear);
            }
            else
            {
                _enemyTarget = null;
            }
        }
        public bool isLeft = false;
        public void CheckFlixEnemy(Enemy enemy)
        {
            SpriteRenderer spriteRenderer = _imageTurret.GetComponent<SpriteRenderer>();
            if (enemy.transform.position.x >= 0)
            {
                spriteRenderer.flipX = true;
            }
            else
            {
                spriteRenderer.flipX = false;
            }
        }
        public void Shooting()
        {
            Enemy enemy = _enemyTarget.GetComponent<Enemy>();
            Bullet bullet = _bullet.Spawn(_barrel.transform.position, Quaternion.identity);
            Vector3 dir = enemy.transform.position - _barrel.transform.position;
            bullet.SetUp(this, dir, _damageTurret);
        }

    }
}

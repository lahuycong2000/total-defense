using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign.Unity;
namespace ZomDefense
{
    public class TurretManager : MonoBehaviour
    {
        public const string PATH_ITEM = "Data/{0}";
        [SerializeField] private int _id;
        [SerializeField] private Canvas _canvasUpdate;
        [SerializeField] private GameObject _newTurret;

        [SerializeField] private List<TurretUpdate> _listTurretNew = new List<TurretUpdate>();
        [SerializeField] private List<TurretUpdate> _listTurretUp = new List<TurretUpdate>();

        [SerializeField] private int _levelTurret;
        [SerializeField] private TurretItem _turret;
        [SerializeField] private SpriteRenderer _plane;
        [SerializeField] private SpriteRenderer _cot;

        private int _priceCoin;
        private int _index;
        public int Id { get => _id; set => _id = value; }
        public Canvas CanvasUpdate { get => _canvasUpdate; set => _canvasUpdate = value; }
        public GameObject NewTurret { get => _newTurret; set => _newTurret = value; }
        public int LevelTurret { get => _levelTurret; set => _levelTurret = value; }
        public int PriceCoin { get => _priceCoin; set => _priceCoin = value; }

        private List<string> nameUp = new List<string> { "Up", "Sell" };

        public void Start()
        {
            _levelTurret = 0;
            _index = 0;
            _id = 0;
            _listTurretUp[0].gameObject.SetActive(false);
            _listTurretUp[1].gameObject.SetActive(false);
            SetDataTurret(0);
        }

        public void TurretLevelUp()
        {
            _levelTurret++;
            if (_levelTurret == 1 && _index < 1)
            {
                _turret = GameManager.Instance.SpawnTurret(new Vector3(this.transform.position.x, this.transform.position.y + 0.3f, this.transform.position.z));
                // _plane.gameObject.SetActive(false);
                 _cot.gameObject.SetActive(false);
            }
            DataTurret data = Database.Instance.turrets[_id].dataTurret[_levelTurret - 1];
            // _turret.Init(LoadSprite(PATH_ITEM, data.image), data.range, data.damage, data.timeshoot);
            _turret.Init(data._imageTurret, data.range, data.damage, data.timeshoot, data.indexBullet);
            ChecklevelTurret();
        }

        public void SetDataTurret(int number)
        {
            _listTurretUp[0].gameObject.SetActive(false);

            SetActiveUI(true);
            for (int i = 0; i < _listTurretNew.Count; i++)
            {
                DataTurret data = Database.Instance.turrets[i + number].dataTurret[_levelTurret];
                //_listTurretNew[i].Init(i + number, LoadSprite(PATH_ITEM, Database.Instance.turrets[i + number].nameTurret), data.coin);
                _listTurretNew[i].Init(i + number, Database.Instance.turrets[i + number].dataTurret[0]._imageMini, data.coin);
                _priceCoin = data.coin;
            }
        }
        public void ChecklevelTurret()
        {
            _listTurretUp[1].PriceTurret = Database.Instance.turrets[_id].dataTurret[_levelTurret - 1].priceSell;
            SetUp(1);
            switch (_levelTurret)
            {
                case 3:
                    //Debug.Log("============");
                    CheckIdTurret();
                    //_index++;
                    break;
                default:
                    //Debug.Log(">>>>>>>>>>>");
                    _listTurretUp[0].IdTurret = _id;
                    SetDataTurretUp();
                    break;
            }
        }
        public void CheckIdTurret()
        {
            _levelTurret = 0;
            // switch (_id)
            // {
            //     case 0:
            //         SetDataTurret(3);
            //         break;
            //     case 1:
            //         SetDataTurret(6);
            //         break;
            //     case 2:
            //         SetDataTurret(9);
            //         break;
            //     default:
            _listTurretUp[0].gameObject.SetActive(false);
            //         break;
            // }
        }
        public void SetDataTurretUp()
        {
            SetActiveUI(false);
            DataTurret data = Database.Instance.turrets[_id].dataTurret[_levelTurret];
            _listTurretUp[0].PriceTurret = data.coin;
            _priceCoin = data.coin;
            SetUp(0);
        }
        public void SellTurret(int number)
        {
            UIManager.Instance.AddCoin(number);
            ResetTurret();
        }
        public void SetUp(int number)
        {
            _listTurretUp[number].gameObject.SetActive(true);
            _listTurretUp[number].UIcoin();
        }
        public void SetActiveUI(bool isTrue)
        {
            foreach (var item in _listTurretNew)
            {
                item.gameObject.SetActive(isTrue);
            }
        }
        private Sprite LoadSprite(string pathType, string nameImage)
        {
            string path = string.Format(pathType, nameImage);
            Sprite sprite = Resources.Load<Sprite>(path);
            return sprite;
        }
        public void ResetTurret()
        {
            Start();
            if (_turret != null)
            {
                _turret.Recycle();
            }
        }

    }
}
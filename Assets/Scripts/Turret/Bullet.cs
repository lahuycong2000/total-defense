using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ensign.Unity;

namespace ZomDefense
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float _speed;
        private Vector3 _shootDir;
        private TurretItem _turret;
        private GameObject _barrel;
        private int _damageTurret;
        [SerializeField] private bool _isSlow;
        [SerializeField] private SpriteRenderer _imageBullet;
        public SpriteRenderer ImageBullet { get => _imageBullet; set => _imageBullet = value; }

        public void SetUp(TurretItem turret, Vector3 shootDir, int damage)
        {
            _turret = turret;
            _damageTurret = damage;
            _shootDir = shootDir.normalized;
        }
        public void Update()
        {
            transform.position += _shootDir * (_speed / 10f) * Time.deltaTime;
            if (Vector2.Distance(this.transform.position, _turret.transform.position) > 5)
            {
                this.Recycle();
            }
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            if (enemy)
            {
                enemy.TakeDamage(_damageTurret, _isSlow);
                this.Recycle();
            }
        }
    }
}

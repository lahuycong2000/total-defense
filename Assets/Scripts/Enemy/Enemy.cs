using System;
using System.Collections;
using System.Collections.Generic;
using Ensign.Unity;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ZomDefense
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _imageEnemy;
        [SerializeField] private int _enemyHealth;
        [SerializeField] private float _enemySpeed;
        [SerializeField] private Animator _animator;
        [SerializeField] private GameObject _effectBlood;
        private int _pointIndex;
        [SerializeField] private Slider _health;
        private int _coin;
        private bool isEnemySlow;


        public float EnemySpeed { get => _enemySpeed; set => _enemySpeed = value; }
        public Animator Animator { get => _animator; set => _animator = value; }
        public int EnemyHealth
        {
            get
            {
                return _enemyHealth;
            }
            set
            {
                _enemyHealth = value;
                if (_enemyHealth <= 0)
                {
                    EnemyDie();
                }
            }
        }
        public void Init(Sprite image, int health, float speed, int coin)
        {
            _imageEnemy.sprite = image;
            _enemyHealth = health;
            _enemySpeed = speed;
            _coin = coin;

        }

        public void Start()
        {
            _health.value = _enemyHealth;
            _pointIndex = 0;
            EnemyMove(_pointIndex);
            //_textHealth.text = CurrentHealth.ToString();
            //gameObject.AddComponent<PolygonCollider2D>();
        }

        public void Update()
        {
            if (GameManager.Instance.GameState == GameManager.EGameState.Playing)
            {
                ControllerEnemy();
            }
        }
        public void ControllerEnemy()
        {
            EnemyMove(_pointIndex);
            Vector3 dir = GameManager.Instance.LvlManager.ListPoint[_pointIndex].position - transform.position;
            //float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            //_imageEnemy.transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);
            if (Vector2.Distance(this.transform.position, GameManager.Instance.LvlManager.ListPoint[_pointIndex].position) < 0.1f)
            {
                if (_pointIndex < GameManager.Instance.LvlManager.ListPoint.Count - 1)
                {
                    _pointIndex++;
                    //EnemyMove(_pointIndex);
                }
                else
                {
                    //_enemySpeed = 5;
                    EnemyDie();
                    GameManager.Instance.HeartDie();
                }
            }
        }

        public void EnemyMove(int number)
        {
            transform.position = Vector2.MoveTowards(transform.position, GameManager.Instance.LvlManager.ListPoint[number].transform.position, _enemySpeed * Time.deltaTime);
            SpriteRenderer spriteRenderer = _imageEnemy.GetComponent<SpriteRenderer>();
            if (GameManager.Instance.LvlManager.ListPoint[number].transform.position.x <= transform.position.x)
            {
                spriteRenderer.flipX = true;
            }
            else
            {
                spriteRenderer.flipX = false;
            }
            //TweenMove(this, GameManager.Instance.ListPoint[number].transform.position, _enemySpeed);
        }
        public void TweenMove(Enemy enemy, Vector3 vector3, float time)
        {
            enemy.gameObject.transform.TweenMove(vector3, time);

        }
        public void TakeDamage(int damage, bool isSlow)
        {
            // float percentage = 60f;
            if (isSlow && isEnemySlow == false)
            {
                //Debug.Log("1");
                // float percentValue = (percentage / 100f) * _enemySpeed;
                // _enemySpeed = _enemySpeed - (int)percentValue;
                _enemySpeed -= 0.5f;
                isEnemySlow = true;
                this.ActionWaitTime(3, () =>
                {
                    // _enemySpeed = _enemySpeed + (int)percentValue;
                    _enemySpeed += 0.5f;
                    isEnemySlow = false;
                });
            }
            _enemyHealth = _enemyHealth - damage;
            _health.value -= damage;

            EffectBlood();
            if (_enemyHealth <= 0)
            {
                EnemyDie();
            }
        }
        public void EffectBlood()
        {
            //GameObject go = _effectBlood.Spawn(this.transform,this.transform.position);
            _effectBlood.gameObject.SetActive(true);
            this.ActionWaitTime(0.5f, () =>
                {
                    _effectBlood.gameObject.SetActive(false);
                    // this.Recycle();
                });
        }
        public void EnemyDie()
        {
            UIManager.Instance.AddCoin(_coin);
            GameManager.Instance.ListEnemy.Remove(this);
            this.Recycle();
        }


    }
}
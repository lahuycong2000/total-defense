using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign.Unity;
using System.Collections;
using UnityEngine.EventSystems;
using TMPro;

namespace DoWork
{
    public class SpinManager : MonoBehaviour, IPointerDownHandler
    {

        public const string PATH_ITEM = "Main/{0}";
        public const float CIRCLE = 360f;

        [Header("WHEEL_DATA")]

        [SerializeField] private int _countSpin;
        [SerializeField] private float _timeRotate;
        [SerializeField] private float _numberCircleRotate;
        [SerializeField] private List<Gift> _listGift = new List<Gift>();
        [SerializeField] private Gift _prefabGift;
        [SerializeField] private GameObject _dataGift;
        [SerializeField] private AnimationCurve _curve;
        [SerializeField] private GameObject _cardJob;
        [SerializeField] private TextMeshProUGUI _textJob;

        private float _angleOfOneObject;
        private float _currentTime;
        private int _currentDataIndex;
        private bool _isRunWheel;
        public bool IsRunWheel { get => _isRunWheel; set => _isRunWheel = value; }

        void Start()
        {
            _angleOfOneObject = CIRCLE / _countSpin;
            InstantiateGift();
            _currentDataIndex = _countSpin;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (_currentTime == 0)
            {
                StartRotate();
            }
            else
            {
                return;
            }
        }
        public void InstantiateGift()
        {
            for (int i = 0; i < _countSpin; i++)
            {
                Gift newGift = Instantiate<Gift>(_prefabGift, _dataGift.transform);
                newGift.transform.localPosition = Vector3.zero;
                _dataGift.transform.GetChild(i).eulerAngles = new Vector3(0, 0, -CIRCLE / _countSpin * i);
                //newGift.Initialize(i, PATH_ITEM, GameManager.Instance.LoadSaveJson.IconWheel[i]);
                _listGift.Add(newGift);
            }
        }
        public void StartRotate()
        {
            // if (GameManager.Instance.ManagerEnergy.TotalEnergy > 0)
            // {
            //     StartCoroutine(RotateWheel());
            // }
        }
        IEnumerator RotateWheel()
        {
            _isRunWheel = true;
            //GameManager.Instance.ManagerEnergy.UseEnergy();
            float starAngle = transform.eulerAngles.z;
            int increaseCell = UnityEngine.Random.Range(1, _countSpin);
            _currentDataIndex += increaseCell;
            _currentDataIndex = _currentDataIndex % _countSpin;
            //Debug.LogError(_listGift[_currentDataIndex].Data);
            float angle = (_numberCircleRotate * CIRCLE) + _angleOfOneObject * increaseCell;
            while (_currentTime < _timeRotate)
            {
                yield return new WaitForEndOfFrame();
                _currentTime += Time.deltaTime;
                float angleCurrent = angle * _curve.Evaluate(_currentTime / _timeRotate);
                this.transform.eulerAngles = new Vector3(0, 0, angleCurrent + starAngle);
            }
            this.ActionWaitTime(1, () =>
            {
                SetGift(_listGift[_currentDataIndex].Data);
                _currentTime = 0;
                _isRunWheel = false;
            });
        }
        public void SetGift(int dataWheel)
        {
            // UserData data = GameManager.Instance.LoadSaveJson.LoadFromJson();
            // switch (dataWheel)
            // {
            //     case 1:
            //     case 5:
            //     case 9:
            //         TakeJob(GameManager.Instance.LoadSaveJson.ListJobLow, data.valueJobLow[0] * 1000, data.valueJobLow[1] * 1000);
            //         CloseCard();
            //         //Debug.Log("TakeJobLow");
            //         break;
            //     case 2:
            //     case 6:
            //     case 10:
            //         GameManager.Instance.ManagerEnergy.AddEnergy(GameManager.Instance.RandomBase(data.valueEnegry[0], data.valueEnegry[1]));
            //         //Debug.Log("TakeEnergy");
            //         break;
            //     case 3:
            //     case 7:
            //     case 11:
            //         //Debug.Log("GoSteal");
            //         UIManager.Instance.ChangeTab(EMainUI.StealScene);
            //         break;
            //     default:
            //         TakeJob(GameManager.Instance.LoadSaveJson.ListJobHigh, data.valueJobHigh[0] * 1000, data.valueJobHigh[1] * 1000);
            //         CloseCard();
            //         //Debug.Log("TakeJobHigh");
            //         break;
            //}
        }
        public void TakeJob(List<int> list, int min, int max)
        {
            //int coin = GameManager.Instance.RandomBase(min, max);
            //SetActiveCard(list, coin);
            //GameManager.Instance.ManagerCoin.AddCoin(coin);
        }
        public void SetActiveCard(List<int> list, int number)
        {
            _cardJob.gameObject.SetActive(true);
            //_textJob.text = "Get job : " + SetTextWheelData(list) + $"  {number:#,0} đ";
        }
        public void CloseCard()
        {
            this.ActionWaitTime(2, () =>
            {
                _cardJob.gameObject.SetActive(false);
            });
        }
        // public string SetTextWheelData(List<int> list)
        // {
        //     // WheelData wheelData = GameManager.Instance.LoadSaveJson.WheelData;
        //     // int random = UnityEngine.Random.Range(0, list.Count);
        //     // // Debug.Log("run " + random);
        //     // string name = wheelData.jobWheelDatas[random].textJob;
        //     // return name;
        // }

    }
}

using System;
using System.Collections.Generic;
using UnityEngine;
using Ensign.Unity;

public class Database : Singleton<Database>
{
    public Turret[] turrets;
    public DataEnemy[] enemys;
    public Level[] levels;
}

[Serializable]
public class Turret
{
    public string nameTurret;
    public int typeTurret;
    public string info;
    public DataTurret[] dataTurret;
}
[Serializable]
public class DataTurret
{
    public int lvl;
    public int coin;
    public string image;
    public int damage;
    public float range;
    public float timeshoot;
    public int priceSell;
    public int indexSkill;
    public Sprite _imageTurret;
    public Sprite _imageMini;
    public int indexBullet;
  
}
[Serializable]
public class DataEnemy
{
    public string nameEnemy;
    public string image;
    public int health;
    public float speed;
    public int coin;
    public int indexSkill;
    public string info; 
    public Sprite _imageEnemy;
}
[Serializable]
public class Level
{
    public string nameLv;
    public int startCoin;
    public Wave[] waves;
}
[Serializable]
public class Wave
{
    public IdEnemy[] idEnemies;
}
[Serializable]
public class IdEnemy
{
    public int idEnemy;
    public int count;
}



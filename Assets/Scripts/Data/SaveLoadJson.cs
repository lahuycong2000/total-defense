using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Ensign;
using UnityEngine.SceneManagement;

namespace DoWork
{
    public class SaveLoadJson : MonoBehaviour
    {
        public const string PATH_USER_OTHERS = "Data/UserDataOther";
        public const string PATH_WHEEL_DATA = "Main/WheelData";
        public const string PATH_IMAGE_DATA = "Turret";


        [Header("DATA_LIST")]
        [SerializeField] private List<int> _listJobLow;
        [SerializeField] private List<int> _listJobHigh;
        [SerializeField] private List<string> _iconWheel;
        [SerializeField] private List<string> _imageItem;
        [SerializeField] private List<int> _priceItem;



        [SerializeField] private List<ListDataTurret> _listTurretData;



        //private UserDataOther _userDataOther;
        private WheelData _wheelData;
        private TurretData _turretData;

        //public UserDataOther userDataOther { get => _userDataOther; set => _userDataOther = value; }
        public WheelData WheelData { get => _wheelData; set => _wheelData = value; }
        // public ItemData ItemData { get => _itemData; set => _itemData = value; }

        public void AddNew()
        {

        }
        public void AddFirstPlay()
        {

        }
        public void SaveToJson(UserData data)
        {
            string json = data.ToJsonFormat();
            PlayerPrefs.SetString("LeaderBoard", json);
            //GameManager.Instance.ManagerCoin.UpdateInfo();
        }
        public UserData LoadFromJson()
        {
            string json = PlayerPrefs.GetString("LeaderBoard", "");
            UserData data = string.IsNullOrEmpty(json) ? new UserData() : json.JsonToObject<UserData>();
            //Debug.Log(json);
            return data;
        }
        public int TakeCoin()
        {
            int number = LoadFromJson().coinUser;
            return number;
        }
        public string TakeName()
        {
            string text = LoadFromJson().nameUser;
            return text;
        }
        public int TakelevelHome()
        {
            int number = LoadFromJson().levelHome;
            return number;
        }
        public void SaveNewData(int numberCoin, int numberItem)
        {
            UserData data = LoadFromJson();
            data.coinUser = numberCoin;
            if (numberItem != 0)
            {
                data.unlockedId.Add(numberItem);
            }
            SaveToJson(data);
        }
        public void AddName(string name)
        {
            UserData data = LoadFromJson();
            data.nameUser = name;
            SaveToJson(data);
        }
        public void LevelUp(int number)
        {
            UserData data = LoadFromJson();
            if (data.levelHome == 2)
            {
                data.levelHome = 1;
            }
            else
            {
                data.levelHome += number;
            }
            SaveToJson(data);
        }
        public void ResetUserData()
        {
            UserData data = new UserData();
            SaveToJson(data);
        }
        public void ResetUnlockedId()
        {
            UserData data = LoadFromJson();
            data.unlockedId.Clear();
            SaveToJson(data);

            _imageItem.Clear();
            _priceItem.Clear();
            ReadDataImageItem();
        }

        public void ReadDataUsersOther()
        {
            var dataJson = Resources.Load<TextAsset>(PATH_USER_OTHERS);
            //_userDataOther = dataJson.text.JsonToObject<UserDataOther>();
        }
        public void ReadWheelData()
        {
            var dataJson = Resources.Load<TextAsset>(PATH_WHEEL_DATA);
            _wheelData = dataJson.text.JsonToObject<WheelData>();
            for (int i = 0; i < WheelData.jobWheelDatas.Length; i++)
            {
                if (WheelData.jobWheelDatas[i].lowOrHigh == 0)
                {
                    _listJobLow.Add(i);
                }
                else
                {
                    _listJobHigh.Add(i);
                }
            }
            for (int j = 0; j < WheelData.imageWheelDatas.Length; j++)
            {
                _iconWheel.Add(WheelData.imageWheelDatas[j].nameImage);
            }
        }
        public void ReadDataImageItem()
        {
            var dataJson = Resources.Load<TextAsset>(PATH_IMAGE_DATA);
            _turretData = dataJson.text.JsonToObject<TurretData>();
            for (int i = 0; i < _turretData.turrets[0].turretInfo.Length; i++)
            {
                // _imageItem.Add(ItemData.houseLv[TakelevelHome() - 1].furnitures[i].nameImage);
                // _priceItem.Add(ItemData.houseLv[TakelevelHome() - 1].furnitures[i].price * 1000);
            }
        }
        public class ListDataTurret
        {
            int lvl;
            string nameTurret;
            int coin;
            string image;
            int damage;
            float range;
            float timeshoot;
            int priceSell;
            int indexSkill;
            string info;
        }
    }

}

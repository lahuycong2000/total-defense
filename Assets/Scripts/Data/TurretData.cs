using System;
using System.Collections.Generic;
using UnityEngine;
using Ensign.Unity;

public class TurretData: Singleton<TurretData>
{
    public Turrets[] turrets;
    public Enemy[] enemys;
}
[Serializable]
public class Turrets
{
    public int typeTurret;
    public TurretInfo[] turretInfo;
}
[Serializable]
public class TurretInfo
{
    public int lvl;
    public string nameTurret;
    public int coin;
    public string image;
    public int damage;
    public float range;
    public float timeshoot;
    public int priceSell;
    public int indexSkill;
    public string info;

}
[Serializable]
public class Enemy
{
    public string nameEnemy;
    public int health ;
    public float speed;
    public int indexSkill;
    public string info;
}
